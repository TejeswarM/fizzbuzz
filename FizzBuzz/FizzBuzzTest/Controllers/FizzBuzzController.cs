﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzController.cs" company="TCS">
//     Company copyright TCS.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzzTest.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using FizzBuzzTest.Controllers;
    using FizzBuzzTest.Models;
    using Repository;
    using PagedList;

    public class FizzBuzzController : Controller
    {
       
        /// <summary>
        /// Number Elements to be displayed per page
        /// </summary>
        private int _numberOfRecordsPerPage = 20;

        /// <summary>
        /// FizzBuzz Logic 
        /// </summary>
        private readonly IGetData _getData;
     
        /// <summary>
        /// FizzBuzzTestController Constructor.
        /// </summary>
        /// <param name="getData">validationForFizzBuzzLogic.</param>
        /// <param name="dataFromRepository">dataFromRepository.</param>
        public FizzBuzzController(IGetData getData)
        {
            this._getData = getData;
            
        }

        /// <summary>
        /// Returns ResultView View.
        /// </summary>
        /// <param name="model">model object of type FizzBuzzModel</param>
        /// <param name="page">page of type Integer</param>
        /// <returns>Returns ResultView View</returns>
        [HttpGet]
        public ActionResult ResultView(FizzBuzz model,int page = 1)
        {
            var repoData =_getData.GetResult(model.numberGivenByUser);
            if (repoData.Count != 0)
            {
                model.paging = repoData.ToPagedList(page, this._numberOfRecordsPerPage);
                model.numberGivenByUser = repoData.Count;
            }
            return View("ResultView", model);
        }

        /// <summary>
        /// Set the model object
        /// </summary>
        /// <param name="model">model object of type FizzBuzzModel</param>
        /// <param name="page">page of type Integer</param>
        /// <returns>Returns ResultView View</returns>
        [HttpPost]
        public ActionResult ResultView(FizzBuzz model, int? page)
        {
            int pageNo = page ?? 1;
            if (ModelState.IsValid)
            {
                var outPutList = _getData.GetResult(model.numberGivenByUser);
                if (outPutList != null)
                {
                    model.paging = outPutList.ToPagedList(pageNo, this._numberOfRecordsPerPage);
                }
            }
            return View("ResultView", model);
        }
    }
}