﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzz.cs" company="TCS">
//     Company copyright TCS.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzzTest.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.ComponentModel.DataAnnotations;
    using PagedList;


    public class FizzBuzz
    {

        /// <summary>
        /// Gets or sets Given Input Number
        /// </summary>
        [Required(ErrorMessage = "Please Enter Any Number")]
        [Range(1, 1000, ErrorMessage = "Given Number Should Be Between 1 And 1000")]
        [Display(AutoGenerateField = false,Name = "Enter Number")]

        public int numberGivenByUser { get; set; }


        /// <summary>
        /// Gets or sets list of elements for pagination in view.
        /// </summary>
        public IPagedList<string> paging { get; set; }

        
    }
}