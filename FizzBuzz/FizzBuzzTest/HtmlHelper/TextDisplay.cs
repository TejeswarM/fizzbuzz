﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FizzBuzzTest.HtmlHelper
{
    public class TextDisplay
    {
        /// <summary>
        /// TxtFormat Method.
        /// </summary>
        /// <param name="text">text</param>
        /// <returns>HtmlString</returns>
        public static IHtmlString TxtFormat(string text)
        {
            string blueText = "<label style='color:blue'>{0}</label>";
            string greenText = "<label style='color:Green'>{0}</label>";
            text = text.Replace("Fizz", string.Format(blueText, "fizz"));
            text = text.Replace("Buzz", string.Format(greenText, "buzz"));
            text = text.Replace("Wizz", string.Format(blueText, "wizz"));
            text = text.Replace("Wuzz", string.Format(greenText, "wuzz"));
            return new HtmlString(text);
        }
    }
}
