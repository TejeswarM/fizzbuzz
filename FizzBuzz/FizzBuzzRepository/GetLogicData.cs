﻿//-----------------------------------------------------------------------
// <copyright file="GetData.cs" company="TCS">
//     Company copyright TCS.
// </copyright>
//-----------------------------------------------------------------------
namespace Repository
{
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.DisplayText;

    /// <summary>
    /// GetData class to implement fizzbuzz logic.
    /// </summary>
    public class GetLogicData : IGetData
    {
        /// <summary>
        /// IDayValidation inetrface intialization.
        /// </summary>
        private readonly IDayValidation _dayValidation;

        /// <summary>
        /// ITextService inetrface intialization.
        /// </summary>
        private readonly ITextService _textService;

        /// <summary>
        /// GetData constructor.
        /// </summary>
        /// <param name="dayValidation">dayValidation</param>
        /// <param name="textService">textService</param>
        
        public GetLogicData(IDayValidation dayValidation, ITextService textService)
        {
            this._dayValidation = dayValidation;
            this._textService = textService;
        }

        /// <summary>
        /// GetResult method to get result for logic implemented.
        /// </summary>
        /// <param name="num">num</param>
        /// <returns>list of string</returns>
        public IList<string> GetResult(int num)
        {
             List<string> numList = new List<string>();
             for (var count = 1; count <= num; count++)
             {
                 string result = string.Empty;
                 string resultValidatiion = _dayValidation.GetCurrentDay();
                 if (count % 15 == 0)
                 {
                     result += resultValidatiion == _textService.RuleForDay() ? _textService.DisplayWizzWuzz() : _textService.DisplayFizzBuzz();
                 }
                 else if (count % 5 == 0)
                 {
                     result += resultValidatiion == _textService.RuleForDay() ? _textService.DisplayWuzz() : _textService.DisplayBuzz();
                 }
                 else if (count % 3 == 0)
                 {
                     result += resultValidatiion == _textService.RuleForDay() ? _textService.DisplayWizz() : _textService.DisplayFizz();
                 }
                 else
                 {
                     result = count.ToString();
                 }
                 numList.Add(result);
             }

             return numList;
        }       
    }
}
