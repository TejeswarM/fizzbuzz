﻿//-----------------------------------------------------------------------
// <copyright file="IFizz.cs" company="TCS">
//     Company copyright TCS.
// </copyright>
//-----------------------------------------------------------------------
namespace Repository.DisplayText
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// IFizz interface.
    /// </summary>
    public interface ITextService
    {
        /// <summary>
        /// DisplayFizz method.
        /// </summary>
        /// <returns>string</returns>
        string DisplayFizz();

        // <summary>
        /// DisplayBuzz method.
        /// </summary>
        /// <returns>string</returns>
        string DisplayBuzz();

        /// <summary>
        /// DisplayFizzBuzz method.
        /// </summary>
        /// <returns>string</returns>
        string DisplayFizzBuzz();

        /// <summary>
        /// DisplayWizz method.
        /// </summary>
        /// <returns>string</returns>
        string DisplayWizz();

        /// <summary>
        /// DisplayWuzz method.
        /// </summary>
        /// <returns>string</returns>
        string DisplayWuzz();

        /// <summary>
        /// DisplayWizzWuzz method.
        /// </summary>
        /// <returns>string</returns>
        string DisplayWizzWuzz();

        /// <summary>
        /// FizzBuzzDayRule method.
        /// </summary>
        /// <returns>string</returns>
        string RuleForDay();
    }
}
