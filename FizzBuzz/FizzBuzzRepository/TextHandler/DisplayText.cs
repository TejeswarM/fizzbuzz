﻿//-----------------------------------------------------------------------
// <copyright file="DisplayText.cs" company="TCS">
//     Company copyright TCS.
// </copyright>
//-----------------------------------------------------------------------
namespace Repository.DisplayText
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Fizz class.
    /// </summary>
    public class DisplayText : ITextService
    {
        /// <summary>
        /// DisplayFizz method.
        /// </summary>
        /// <returns>Fizz</returns>
        public string DisplayFizz()
        {
            return "Fizz";
        }


        public string DisplayBuzz()
        {
            return "Buzz";
        }

        public string DisplayFizzBuzz()
        {
            return "Fizz Buzz";
        }

        public string DisplayWizz()
        {
            return "Wizz";
        }

        public string DisplayWuzz()
        {
            return "Wuzz";
        }

        public string DisplayWizzWuzz()
        {
            return "Wizz Wuzz";
        }

        public string RuleForDay()
        {
            return "Wednesday";
        }
    }
}
