﻿//-----------------------------------------------------------------------
// <copyright file="ValidDay.cs" company="TCS">
//     Company copyright TCS.
// </copyright>
//-----------------------------------------------------------------------
namespace Repository
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// ValidDay class  
    /// </summary>
    public class CurrentDay : IDayValidation
    {
        /// <summary>
        /// RuleForTheDay menthod to return Wednesday.
        /// </summary>
        /// <returns>Wednesday</returns>
        public string GetCurrentDay()
        {
            return DateTime.Today.DayOfWeek.ToString();
        }
    }
}
