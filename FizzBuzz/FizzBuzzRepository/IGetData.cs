﻿//-----------------------------------------------------------------------
// <copyright file="IGetData.cs" company="TCS">
//     Company copyright TCS.
// </copyright>
//-----------------------------------------------------------------------
namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    /// <summary>
    /// IGetData interface.
    /// </summary>
    public interface IGetData
    {
        /// <summary>
        /// GetResult method to reurn list.
        /// </summary>
        /// <param name="num">num</param>
        /// <returns> list of string</returns>
        IList<string> GetResult(int num);
    }
}
