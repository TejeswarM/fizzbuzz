﻿//-----------------------------------------------------------------------
// <copyright file="IDayValidation.cs" company="TCS">
//     Company copyright TCS.
// </copyright>
//-----------------------------------------------------------------------
namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// IDayValidation interface.
    /// </summary>
    public interface IDayValidation
    {
        /// <summary>
        /// RuleForTheDay method.
        /// </summary>
        /// <returns>string</returns>
        string GetCurrentDay();
    }
}
