﻿//-----------------------------------------------------------------------
// <copyright file="DataTest.cs" company="TCS">
//     Company copyright TCS.
// </copyright>
//-----------------------------------------------------------------------
namespace Repository.UnitTest
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using NUnit.Framework;
    using Repository;
    using Rhino.Mocks;
    using Repository.DisplayText;

    /// <summary>
    /// RepositoryTest Class.
    /// </summary>
    [TestFixture]
    public class DataTest
    {
        /// <summary>
        /// This method takes number 5 as input and give output 5 numbers,day is Monday.
        /// </summary>
        [Test]
        public void Given_number_5_result_with_5_numbers_result()
        {
            var repository=MockRepository.GenerateMock<IGetData>();
            var dayValidation=MockRepository.GenerateMock<IDayValidation>();
            var Text=MockRepository.GenerateMock<ITextService>();
            GetLogicData getdata = new GetLogicData(dayValidation, Text);
            dayValidation.Stub(p => p.GetCurrentDay()).IgnoreArguments().Return("Monday");
            Text.Stub(p => p.DisplayFizz()).Return("Fizz");
            Text.Stub(p => p.DisplayBuzz()).Return("Buzz");
            Text.Stub(p => p.DisplayFizzBuzz()).Return("Fizz Buzz");
            Text.Stub(p => p.DisplayWizz()).Return("Wizz");
            Text.Stub(p => p.DisplayWuzz()).Return("Wuzz");
            Text.Stub(p => p.DisplayWizzWuzz()).Return("Wizz Wuzz");
            Text.Stub(p => p.RuleForDay()).IgnoreArguments().Return("Wednesday");
            repository.Stub(p => p.GetResult(5)).Return(new List<string> { "1", "2", "Fizz", "4", "Buzz" });
            IList<string> lst = getdata.GetResult(5);
            Assert.AreEqual(5, lst.Count);
            Assert.AreEqual("1", lst[0]);
            Assert.AreEqual("2", lst[1]);
            Assert.AreEqual("Fizz", lst[2]);
            Assert.AreEqual("4", lst[3]);
            Assert.AreEqual("Buzz", lst[4]);
           
        }

        /// <summary>
        /// This method takes number 15 as input and gives output 15 numbers for divisible by 3 and day is Tuesday.
        /// </summary>
        [Test]
        public void Given_number_15_divisible_by_3_result()
        {
            var repository = MockRepository.GenerateMock<IGetData>();
            var dayValidation = MockRepository.GenerateMock<IDayValidation>();
            var Text = MockRepository.GenerateMock<ITextService>();
            GetLogicData getdata = new GetLogicData(dayValidation, Text);
            dayValidation.Stub(p => p.GetCurrentDay()).IgnoreArguments().Return("Tuesday");
            Text.Stub(p => p.DisplayFizz()).Return("Fizz");
            Text.Stub(p => p.DisplayBuzz()).Return("Buzz");
            Text.Stub(p => p.DisplayFizzBuzz()).Return("Fizz Buzz");
            Text.Stub(p => p.DisplayWizz()).Return("Wizz");
            Text.Stub(p => p.DisplayWuzz()).Return("Wuzz");
            Text.Stub(p => p.DisplayWizzWuzz()).Return("Wizz Wuzz");
            Text.Stub(p => p.RuleForDay()).IgnoreArguments().Return("Wednesday");
            repository.Stub(p => p.GetResult(15)).Return(new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz","11","Fizz","13","14","Fizz Buzz" });
            IList<string> lst = getdata.GetResult(15);
            Assert.AreEqual(15, lst.Count);
            Assert.AreEqual("Fizz", lst[2]);
            Assert.AreEqual("Fizz", lst[5]);
            Assert.AreEqual("Fizz", lst[8]);
            Assert.AreEqual("Fizz", lst[11]);
        }

        /// <summary>
        /// This method takes number 10 as input and gives output 10 numbers for divisible by 5 and day is Friday.
        /// </summary>
        [Test]
        public void Given_number_10_divisible_by_5_result()
        {
            var repository = MockRepository.GenerateMock<IGetData>();
            var dayValidation = MockRepository.GenerateMock<IDayValidation>();
            var Text = MockRepository.GenerateMock<ITextService>();
            GetLogicData getdata = new GetLogicData(dayValidation, Text);
            dayValidation.Stub(p => p.GetCurrentDay()).IgnoreArguments().Return("Friday");
            Text.Stub(p => p.DisplayFizz()).Return("Fizz");
            Text.Stub(p => p.DisplayBuzz()).Return("Buzz");
            Text.Stub(p => p.DisplayFizzBuzz()).Return("Fizz Buzz");
            Text.Stub(p => p.DisplayWizz()).Return("Wizz");
            Text.Stub(p => p.DisplayWuzz()).Return("Wuzz");
            Text.Stub(p => p.DisplayWizzWuzz()).Return("Wizz Wuzz");
            Text.Stub(p => p.RuleForDay()).IgnoreArguments().Return("Wednesday");
            repository.Stub(p => p.GetResult(10)).Return(new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz"});
            IList<string> lst = getdata.GetResult(10);
            Assert.AreEqual(10, lst.Count);
            Assert.AreEqual("Buzz", lst[4]);
            Assert.AreEqual("Buzz", lst[9]);
        }

        /// <summary>
        /// This method takes number 15 as input and gives output 15 numbers for divisible by 3&5,day is Monday.
        /// </summary>
        [Test]
        public void Given_number_15_divisible_by_3And5_result()
        {
            var repository = MockRepository.GenerateMock<IGetData>();
            var dayValidation = MockRepository.GenerateMock<IDayValidation>();
            var Text = MockRepository.GenerateMock<ITextService>();
            GetLogicData getdata = new GetLogicData(dayValidation, Text);
            dayValidation.Stub(p => p.GetCurrentDay()).IgnoreArguments().Return("Monday");
            Text.Stub(p => p.DisplayFizz()).Return("Fizz");
            Text.Stub(p => p.DisplayBuzz()).Return("Buzz");
            Text.Stub(p => p.DisplayFizzBuzz()).Return("Fizz Buzz");
            Text.Stub(p => p.DisplayWizz()).Return("Wizz");
            Text.Stub(p => p.DisplayWuzz()).Return("Wuzz");
            Text.Stub(p => p.DisplayWizzWuzz()).Return("Wizz Wuzz");
            Text.Stub(p => p.RuleForDay()).IgnoreArguments().Return("Wednesday");
            repository.Stub(p => p.GetResult(15)).Return(new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz" });
            IList<string> lst = getdata.GetResult(15);
            Assert.AreEqual(15, lst.Count);
            Assert.AreEqual("Fizz Buzz", lst[14]);
        }

        /// <summary>
        /// This method takes number 5 as input and give output 5 numbers,day is Wednesday.
        /// </summary>
        [Test]
        public void Given_number_5_result_with_5_numbers_with_day_as_wednesday_result()
        {
            var repository = MockRepository.GenerateMock<IGetData>();
            var dayValidation = MockRepository.GenerateMock<IDayValidation>();
            var Text = MockRepository.GenerateMock<ITextService>();
            GetLogicData getdata = new GetLogicData(dayValidation, Text);
            dayValidation.Stub(p => p.GetCurrentDay()).IgnoreArguments().Return("Wednesday");
            Text.Stub(p => p.DisplayFizz()).Return("Fizz");
            Text.Stub(p => p.DisplayBuzz()).Return("Buzz");
            Text.Stub(p => p.DisplayFizzBuzz()).Return("Fizz Buzz");
            Text.Stub(p => p.DisplayWizz()).Return("Wizz");
            Text.Stub(p => p.DisplayWuzz()).Return("Wuzz");
            Text.Stub(p => p.DisplayWizzWuzz()).Return("Wizz Wuzz");
            Text.Stub(p => p.RuleForDay()).IgnoreArguments().Return("Wednesday");
            repository.Stub(p => p.GetResult(5)).Return(new List<string> { "1", "2", "Fizz", "4", "Buzz" });
            IList<string> lst = getdata.GetResult(5);
            Assert.AreEqual(5, lst.Count);
            Assert.AreEqual("1", lst[0]);
            Assert.AreEqual("2", lst[1]);
            Assert.AreEqual("Wizz", lst[2]);
            Assert.AreEqual("4", lst[3]);
            Assert.AreEqual("Wuzz", lst[4]);

        }


        /// <summary>
        /// This method takes number 15 as input and gives output 15 numbers for divisible by 3,day is Wednesday.
        /// </summary>
        [Test]
        public void Given_number_15_divisible_by_3_with_day_as_Wednesday_result_()
        {
            var repository = MockRepository.GenerateMock<IGetData>();
            var dayValidation = MockRepository.GenerateMock<IDayValidation>();
            var Text = MockRepository.GenerateMock<ITextService>();
            GetLogicData getdata = new GetLogicData(dayValidation, Text);
            dayValidation.Stub(p => p.GetCurrentDay()).IgnoreArguments().Return("Wednesday");
            Text.Stub(p => p.DisplayFizz()).Return("Fizz");
            Text.Stub(p => p.DisplayBuzz()).Return("Buzz");
            Text.Stub(p => p.DisplayFizzBuzz()).Return("Fizz Buzz");
            Text.Stub(p => p.DisplayWizz()).Return("Wizz");
            Text.Stub(p => p.DisplayWuzz()).Return("Wuzz");
            Text.Stub(p => p.DisplayWizzWuzz()).Return("Wizz Wuzz");
            Text.Stub(p => p.RuleForDay()).IgnoreArguments().Return("Wednesday");
            repository.Stub(p => p.GetResult(15)).Return(new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz" });
            IList<string> lst = getdata.GetResult(15);
            Assert.AreEqual(15, lst.Count);
            Assert.AreEqual("Wizz", lst[2]);
            Assert.AreEqual("Wizz", lst[5]);
            Assert.AreEqual("Wizz", lst[8]);
            Assert.AreEqual("Wizz", lst[11]);
        }

        /// <summary>
        /// This method takes number 10 as input and gives output 10 numbers for divisible by 5,day is Wednesday.
        /// </summary>
        [Test]
        public void Given_number_10_divisible_by_5_with_day_as_wednesday_result()
        {
            var repository = MockRepository.GenerateMock<IGetData>();
            var dayValidation = MockRepository.GenerateMock<IDayValidation>();
            var Text = MockRepository.GenerateMock<ITextService>();
            GetLogicData getdata = new GetLogicData(dayValidation, Text);
            dayValidation.Stub(p => p.GetCurrentDay()).IgnoreArguments().Return("Wednesday");
            Text.Stub(p => p.DisplayFizz()).Return("Fizz");
            Text.Stub(p => p.DisplayBuzz()).Return("Buzz");
            Text.Stub(p => p.DisplayFizzBuzz()).Return("Fizz Buzz");
            Text.Stub(p => p.DisplayWizz()).Return("Wizz");
            Text.Stub(p => p.DisplayWuzz()).Return("Wuzz");
            Text.Stub(p => p.DisplayWizzWuzz()).Return("Wizz Wuzz");
            Text.Stub(p => p.RuleForDay()).IgnoreArguments().Return("Wednesday");
            repository.Stub(p => p.GetResult(10)).Return(new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz" });
            IList<string> lst = getdata.GetResult(10);
            Assert.AreEqual(10, lst.Count);
            Assert.AreEqual("Wuzz", lst[4]);
            Assert.AreEqual("Wuzz", lst[9]);
        }

        /// <summary>
        /// This method takes number 15 as input and gives output 15 numbers for divisible by 3&5,day is Wednesday.
        /// </summary>
        [Test]
        public void Given_number_15_divisible_by_3And5_with_day_as_wednesday_result()
        {
            var repository = MockRepository.GenerateMock<IGetData>();
            var dayValidation = MockRepository.GenerateMock<IDayValidation>();
            var Text = MockRepository.GenerateMock<ITextService>();
            GetLogicData getdata = new GetLogicData(dayValidation, Text);
            dayValidation.Stub(p => p.GetCurrentDay()).IgnoreArguments().Return("Wednesday");
            Text.Stub(p => p.DisplayFizz()).Return("Fizz");
            Text.Stub(p => p.DisplayBuzz()).Return("Buzz");
            Text.Stub(p => p.DisplayFizzBuzz()).Return("Fizz Buzz");
            Text.Stub(p => p.DisplayWizz()).Return("Wizz");
            Text.Stub(p => p.DisplayWuzz()).Return("Wuzz");
            Text.Stub(p => p.DisplayWizzWuzz()).Return("Wizz Wuzz");
            Text.Stub(p => p.RuleForDay()).IgnoreArguments().Return("Wednesday");
            repository.Stub(p => p.GetResult(15)).Return(new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz" });
            IList<string> lst = getdata.GetResult(15);
            Assert.AreEqual(15, lst.Count);
            Assert.AreEqual("Wizz Wuzz", lst[14]);
        }

    }
}