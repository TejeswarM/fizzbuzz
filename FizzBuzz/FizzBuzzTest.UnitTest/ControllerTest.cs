﻿//-----------------------------------------------------------------------
// <copyright file="ControllerTest.cs" company="TCS">
//     Company copyright TCS.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzzTest.UnitTest
{
    using System;
    using NUnit.Framework;
    using FizzBuzzTest.Controllers;
    using FizzBuzzTest.Models;
    using Repository;
    using Rhino.Mocks;
    using System.Web.Mvc;
    using System.Collections.Generic;

    /// <summary>
    /// ControllerTest Class.
    /// </summary>
    [TestFixture]
    public class ControllerTest
    {
        /// <summary>
        /// FizzBuzzController Controller.
        /// </summary>
        FizzBuzzController fizzBuzzController;

        /// <summary>
        /// Get_WhenDataIsNull_ReturnView Method.
        /// </summary>
        [Test]
        public void Get_WhenDataIsNull_ReturnView()
        {
            ////Arrange.
            var controllerLogic = MockRepository.GenerateMock<IGetData>();
            List<String> outputList = new List<string>();
            fizzBuzzController = new FizzBuzzController(controllerLogic);
            controllerLogic.Stub(p => p.GetResult(0)).IgnoreArguments().Return(
                outputList);
            
            ////Act.
            ViewResult result = fizzBuzzController.ResultView(new FizzBuzzTest.Models.FizzBuzz { numberGivenByUser = 0 }, 1) as ViewResult;
            ////Assert.
            Assert.AreEqual(
               "ResultView",
              result.ViewName);
        }


        /// <summary>
        /// Get_WhenDataIsNotNull_SetModel Method.
        /// </summary>
        [Test]
        public void Get_WhenDataIsNotNull_SetModel()
        {
            ////Arrange.
            var controllerLogic = MockRepository.GenerateMock<IGetData>();
            fizzBuzzController = new FizzBuzzController(controllerLogic);
            controllerLogic.Stub(p => p.GetResult(5)).IgnoreArguments().Return(
             new List<string> { "1", "2", "Fizz", "4", "Buzz" });
            ////Act.
            ViewResult result = fizzBuzzController.ResultView(new FizzBuzzTest.Models.FizzBuzz { numberGivenByUser = 5 }, 1) as ViewResult;
            var fizzBuzzData = (FizzBuzzTest.Models.FizzBuzz)result.Model;
            ////Assert.
            Assert.AreEqual(
              5,
              fizzBuzzData.numberGivenByUser);
        }

        /// <summary>
        /// Post_WhenMessageIsNull_ReturnViewResult Method.
        /// </summary>
        [Test]
        public void Post_WhenMessageIsNull_ReturnViewResult()
        {
            ////Arrange.
            var controllerLogic = MockRepository.GenerateMock<IGetData>();
            List<String> outputMessageList = new List<string>();
            var fizzBuzzTest = new FizzBuzzController(controllerLogic);
            controllerLogic.Stub(p => p.GetResult(0)).IgnoreArguments().Return(
                outputMessageList);
            ////Act.
            var result = fizzBuzzTest.ResultView(new FizzBuzzTest.Models.FizzBuzz { numberGivenByUser = 0 }, 1) as ViewResult;
            ////Assert.
            Assert.AreEqual(
               "ResultView",
              result.ViewName);
        }
        /// <summary>
        /// Post_WhenMessageIsNotNull_SetModel Method.
        /// </summary>
        [Test]
        public void Post_WhenMessageIsNotNull_SetModel()
        {
            ////Arrange.
            var controllerLogic = MockRepository.GenerateMock<IGetData>();
            List<String> outputMessageList = new List<string>();
            var fizzBuzzTest = new FizzBuzzController(controllerLogic);
            controllerLogic.Stub(p => p.GetResult(23)).IgnoreArguments().Return(
                outputMessageList);
            ////Act.
            ViewResult returnView = fizzBuzzController.ResultView(new FizzBuzzTest.Models.FizzBuzz { numberGivenByUser = 22 }, 1) as ViewResult;
            ////Assert.
            Assert.AreEqual(
               "ResultView",
              returnView.ViewName);

            var fizzBuzzData = (FizzBuzzTest.Models.FizzBuzz)returnView.Model;
            Assert.AreEqual(fizzBuzzData.numberGivenByUser, 22);
        }
    }
}